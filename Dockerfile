FROM alpine:latest

ADD cloudeve.sh cloudeve.sh

RUN apk add --no-cache --virtual .build-deps ca-certificates curl \
 && chmod +x cloudeve.sh

ENTRYPOINT ["sh", "-c", "cloudeve.sh"]
